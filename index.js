var r = require('random-seed');

module.exports = {
  create: create,
  verify: verify
}

function create(opts = {}) {
  var t0 = opts.t0 || 0;
  var d = opts.d || 20;
  var t1 = new Date().getTime() / 1000 | 0; // Floors the value

  var seed = (t1 - t0) / d | 0;
  return r(seed).intBetween(100000, 999999);
}

function verify(n, opts = {}) {
  return n === create(opts);
}
